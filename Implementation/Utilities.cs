﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TShockAPI;
using TShockAPI.DB;

namespace AdventureQuests
{
    public class Utilities
    {
        public static List<Region> InAreaRegion(int x, int y)
        {
            List<Region> regions = new List<Region>() { };
            List<Region> Regions = TShock.Regions.Regions;
            foreach (Region region in Regions.ToList())
            {
                if (x >= region.Area.Left && x <= region.Area.Right &&
                    y >= region.Area.Top && y <= region.Area.Bottom)
                {
                    regions.Add(region);
                }
            }
            return regions;
        }

        public static string ParamsToSingleString(CommandArgs args, int fromIndex = 0, int paramsToTrimFromEnd = 0)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = fromIndex; i < args.Parameters.Count - paramsToTrimFromEnd; i++)
            {
                if (i > fromIndex)
                    builder.Append(' ');

                builder.Append(args.Parameters[i]);
            }

            return builder.ToString();
        }
    }
}
