﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TShockAPI;
using TShockAPI.DB;

namespace AdventureQuests
{
    public class QuestRegionManager
    {
        private Dictionary<string, QuestRegion> regions;

        public QuestRegionManager()
        {
            regions = new Dictionary<string, QuestRegion>();
        }

        public void ImportRegion(string name, List<QuestInfo> quests, string entryMessage, string exitMessage)
        {
            var reg = TShock.Regions.GetRegionByName(name);
            if (reg == null)
            {
                Console.WriteLine("{0} was not found in tshocks region list.", name);
                return;
            }
            QuestRegion questReg = new QuestRegion(reg, quests, entryMessage, exitMessage);
            if (!regions.ContainsKey(name))
                regions.Add(name, questReg);
        }

        public bool AddRegion(string name)
        {
            if (regions.ContainsKey(name))
            {
                return false;
            }
            var reg = TShock.Regions.GetRegionByName(name);
            QuestRegion questReg = new QuestRegion(reg);
            string entry = string.Concat("Entered Quest Region: ", name);
            string exit = string.Concat("Left Quest Region: ", name);

            AdventureQuestsMain.db.Query(
                    "INSERT INTO QuestRegions (Name, Quests, EntryMessage, ExitMessage) VALUES (@0, @1, @2, @3);",
                    name, "", entry, exit);

            regions.Add(name, questReg);

            return true;
        }

        public bool UpdateRegion(string name)
        {
            if (!regions.ContainsKey(name))
            {
                return false;
            }

            QuestRegion questReg = regions[name];

            AdventureQuestsMain.db.Query(
                    "UPDATE QuestRegions SET Quests=@0, EntryMessage=@1, ExitMessage=@2 WHERE Name=@3", questReg.GetQuestsStr(), questReg.entryMessage, questReg.exitMessage, name);
            return true;
        }

        public QuestRegion GetRegion(string region)
        {
            if (regions.ContainsKey(region))
            {
                return regions[region];
            }

            return null;
        }

        public IEnumerable<string> ListRegions()
        {
            IEnumerable<string> regionNames = from region in regions
                                              select region.Key;
            return regionNames;
        }

        public List<QuestRegion> InRegions(int x, int y)
        {
            List<QuestRegion> ret = new List<QuestRegion>();
            foreach (QuestRegion reg in regions.Values.ToList())
            {
                if (reg.region != null)
                {
                    if (reg.region.InArea(x, y))
                        ret.Add(reg);
                }
            }
            return ret;
        }

        public QuestRegion GetTopRegion(List<QuestRegion> regList)
        {
            QuestRegion ret = null;
            foreach (QuestRegion r in regList)
            {
                if (ret == null)
                    ret = r;
                else
                {
                    if (r.region.Z > ret.region.Z)
                        ret = r;
                }
            }
            return ret;
        }

        public void Clear()
        {
            regions.Clear();
        }
    }
}
