﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Terraria;
using TShockAPI.DB;

namespace AdventureQuests
{
    public class QuestRegion
    {
        public Region region { get; set; }
        public List<QuestInfo> quests { get; set; }
        public string entryMessage { get; set; }
        public string exitMessage { get; set; }

        public QuestRegion(Region r)
        {
            region = r;
        }

        public QuestRegion(Region r, List<QuestInfo> qs, string entryMsg, string exitMsg)
        {
            region = r;
            quests = qs;
            entryMessage = entryMsg;
            exitMessage = exitMsg;
        }

        public bool AddQuest(QuestInfo q)
        {
            if (quests.Contains(q))
                return false;

            quests.Add(q);
            return true;
        }

        public bool RemoveQuest(QuestInfo q)
        {
            if (!quests.Contains(q))
                return false;

            quests.Remove(q);
            return true;
        }

        public string GetQuestsStr() {
            string questStr = "";
            foreach (QuestInfo q in quests)
            {
                if (questStr != "")
                    questStr = string.Join(",", questStr, q.Name);
                else
                    questStr = q.Name;
            }
            return questStr;
        }

        public bool IsInRegion(QuestPlayer plr)
        {
            int x = plr.tsPlayer.TileX;
            int y = plr.tsPlayer.TileY;

            if (region.InArea(x, y))
                return true;

            return false;
        }
    }
}
