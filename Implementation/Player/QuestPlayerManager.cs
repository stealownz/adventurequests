﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TShockAPI;
using TShockAPI.DB;

namespace AdventureQuests
{
    public class QuestPlayerManager
    {
        public QuestPlayer[] players { get; private set; }

        public QuestPlayerManager()
        {
            players = new QuestPlayer[255];
        }

        public void AddPlayer(int index)
        {
            lock (players)
            {
                players[index] = new QuestPlayer(index);
            }
        }

        public void DelPlayer(int index)
        {
            lock (players)
            {
                if (index < 0)
                    return;
                if (players[index] == null)
                    return;

                if (players[index].currentQuest != null)
                    players[index].currentQuest.Stop(false);

                if (players[index].OriginalData != null)
                {
                    players[index].OriginalData.RestoreCharacter(players[index].tsPlayer);
                    players[index].OriginalData = null;
                }

                players[index] = null;
            }
        }

        public void OnUpdate(EventArgs args)
        {
            lock (players)
            {
                foreach (QuestPlayer player in players)
                {
                    if (player == null)
                        continue;

                    List<QuestRegion> qrList = AdventureQuestsMain.regions.InRegions(player.tsPlayer.TileX, player.tsPlayer.TileY);

                    if (!qrList.Any())
                        continue;

                    foreach (QuestRegion qr in qrList.ToList())
                    {
                        if (!player.curQuestRegions.Any())
                        {
                            player.curQuestRegions.Add(qr);
                            player.tsPlayer.SendMessage(qr.entryMessage, Color.Magenta);
                        }

                        else if (!player.curQuestRegions.Contains(qr))
                        {
                            player.curQuestRegions.Add(qr);
                            player.tsPlayer.SendMessage(qr.entryMessage, Color.Magenta);
                        }
                    }

                    foreach (QuestRegion qr in player.curQuestRegions.ToList())
                    {
                        if (!qrList.Any())
                        {
                            player.tsPlayer.SendMessage(qr.exitMessage, Color.Magenta);
                            player.curQuestRegions.Remove(qr);
                        }
                        else if (!qrList.Contains(qr))
                        {
                            player.tsPlayer.SendMessage(qr.exitMessage, Color.Magenta);
                            player.curQuestRegions.Remove(qr);
                        }
                    }
                }
            }
        }

        public QuestPlayer GetPlayer(string name)
        {
            foreach (QuestPlayer plr in players)
            {
                if (plr == null)
                    continue;

                if (plr.tsPlayer.Name == name)
                    return plr;
            }
            return null;
        }

        public QuestPlayer GetPlayer(int index)
        {
            if (index >= 255 || index < 0)
                return null;
            return players[index];
        }

        public bool IsLoggedIn(int index)
        {
            if (index >= 255 || index < 0)
                return false;
            if (players[index] != null)
                return true;

            return false;
        }
    }
}
