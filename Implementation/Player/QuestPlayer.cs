﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TShockAPI;

namespace AdventureQuests
{
    public class QuestPlayer
    {
        public int index;
        public TSPlayer tsPlayer { get { return TShock.Players[index]; } }
        public bool runningQuest;
        public Quest currentQuest;
        public List<QuestRegion> curQuestRegions { get; set; }

        public PlayerData OriginalData = null;
        //public bool TempInv = false;
        //public bool TempHP = false;
        //public bool TempMana = false;
        //public bool TempSpawnPoint = false;

        public bool DropDisabled = false;

        public QuestPlayer(int index)
        {
            this.index = index;
            curQuestRegions = new List<QuestRegion>();
            runningQuest = false;
        }
    }
}
