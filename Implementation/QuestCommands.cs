﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TShockAPI;

namespace AdventureQuests
{
    public class QuestCommands
    {
        #region RootCommand
        public static void RootCommand(CommandArgs args)
        {
            if (args.Parameters.Count > 0)
            {
                switch (args.Parameters[0].ToLower())
                {
                    case "start":
                        StartQuest(args);
                        break;
                    case "stop":
                        StopQuest(args);
                        break;
                    case "list":
                        ListQuests(args);
                        break;
                    case "progress":
                        Progress(args);
                        break;
                    case "give":
                        GiveQuest(args);
                        break;
                    case "forceall":
                        ForceallQuest(args);
                        break;
                    case "help":
                    default:
                        RootCmdHelp(args);
                        break;
                }
            }
            else
                RootCmdHelp(args);
        }

        private static void StartQuest(CommandArgs args)
        {
            if (args.Parameters.Count < 2)
            {
                args.Player.SendMessage("Invalid usage: /quest start [quest name]", Color.Red);
                return;
            }

            QuestPlayer player = AdventureQuestsMain.players.GetPlayer(args.Player.Index);
            List<QuestRegion> regions = player.curQuestRegions;
            QuestInfo qi = AdventureQuestsMain.quests.getQuest(args.Parameters[1]);

            if (player.runningQuest)
            {
                player.tsPlayer.SendMessage("You have a quest running. You may only run one quest at a time.", Color.Red);
                return;
            }

            if (player.tsPlayer.IsLoggedIn)
            {
                if (!regions.Any())
                {
                    player.tsPlayer.SendErrorMessage("You are not in a quest region");
                    return;
                }

                if (qi == null)
                {
                    player.tsPlayer.SendErrorMessage("This quest is non-existant.");
                    return;
                }

                foreach (QuestRegion qr in regions)
                {
                    if (!qr.quests.Contains(qi))
                        continue;

                    QuestManager.StartQuest(player, qi);
                    return;
                }

                args.Player.SendErrorMessage("Quest not found at your location");

                //if (!(q.Permission == ""))
                //{
                //    if (!player.Group.HasPermission(q.Permission))
                //    {
                //        player.SendErrorMessage("You do not have permission to start this quest.");
                //        return;
                //    }
                //}
                //if (QTools.IntervalLeft(player, q) == 0)
                //{
                //    QTools.StartQuest(player, q);

                //    QuestAttemptData lastAttempt = player.MyDBPlayer.QuestAttemptData.Find(x => x.QuestName == q.Name);
                //    if (lastAttempt != null)
                //        lastAttempt.LastAttempt = DateTime.UtcNow;
                //    else
                //    {
                //        QuestAttemptData attempt = new QuestAttemptData(q.Name, false, DateTime.UtcNow);
                //        player.MyDBPlayer.QuestAttemptData.Add(attempt);
                //    }
                //    return;
                //}
                //else
                //{
                //    player.tsPlayer.SendErrorMessage(string.Format("You may not start the quest \"{0}\" for another {1} seconds.", q.Name, QTools.IntervalLeft(player, q).ToString()));
                //    return;
                //}
            }
            else
            {
                player.tsPlayer.SendErrorMessage("You are not Logged in.");
            }
        }

        private static void StopQuest(CommandArgs args)
        {
            QuestPlayer player = AdventureQuestsMain.players.GetPlayer(args.Player.Index);

            if (!player.runningQuest)
            {
                player.tsPlayer.SendErrorMessage("Quest is not running.");
                return;
            }

            player.currentQuest.Stop(false);
        }

        private static void ListQuests(CommandArgs args)
        {
            QuestPlayer player = AdventureQuestsMain.players.GetPlayer(args.Player.Index);
            List<QuestRegion> qrList = player.curQuestRegions;
            if (!player.curQuestRegions.Any())
            {
                args.Player.SendErrorMessage("You are not currently in a quest region.");
                return;
            }

            args.Player.SendInfoMessage("Quests available in region: ");
            StringBuilder str = new StringBuilder();
            foreach (QuestRegion qr in qrList)
            {
                str.Append(string.Concat("Quests: ", qr.GetQuestsStr()));
                args.Player.SendInfoMessage(str.ToString());
                str.Clear();
            }
        }

        private static void Progress(CommandArgs args)
        {
            QuestPlayer player = AdventureQuestsMain.players.GetPlayer(args.Player.Index);
            if (player.currentQuest != null)
                args.Player.SendInfoMessage(player.currentQuest.currentTrigger.Progress());
            else if (player.currentQuest == null)
                args.Player.SendErrorMessage("you have not started a quest.");
        }

        private static void GiveQuest(CommandArgs args)
        {
            if (!args.Player.Group.HasPermission(AdventureQuestsMain.QuestAdmin_Permission))
            {
                args.Player.SendErrorMessage("You do not have access to this command");
                return;
            }

            if (args.Parameters.Count < 3)
            {
                args.Player.SendErrorMessage("Invalid usage: /quest give [player] [quest]");
                return;
            }

            QuestPlayer player = AdventureQuestsMain.players.GetPlayer(args.Parameters[1]);
            QuestInfo qi = AdventureQuestsMain.quests.getQuest(args.Parameters[2]);

            //Add incomplete name handling.
            if (player == null)
            {
                args.Player.SendErrorMessage("Player not found");
                return;
            }

            if (player.runningQuest)
            {
                args.Player.SendErrorMessage("Player has a quest running. He may only run one quest at a time.");
                return;
            }

            if (player.tsPlayer.IsLoggedIn)
            {
                if (qi == null)
                {
                    args.Player.SendErrorMessage("This quest is non-existant.");
                    return;
                }
                
                QuestManager.StartQuest(player, qi);

                args.Player.SendInfoMessage("Player started quest.");
            }
        }

        private static void ForceallQuest(CommandArgs args)
        {
            if (!args.Player.Group.HasPermission(AdventureQuestsMain.QuestAdmin_Permission))
            {
                args.Player.SendErrorMessage("You do not have access to this command");
                return;
            }

            if (args.Parameters.Count < 2)
            {
                args.Player.SendMessage("Invalid usage: /quest forceall [quest]", Color.Red);
                return;
            }

            QuestInfo qi = AdventureQuestsMain.quests.getQuest(args.Parameters[1]);

            foreach (QuestPlayer qp in AdventureQuestsMain.players.players)
            {
                if (qp.runningQuest)
                    qp.currentQuest.Stop(false);

                QuestManager.StartQuest(qp, qi);
            }
        }

        private static void RootCmdHelp(CommandArgs args)
        {
            int pageNumber;
            int pageParamIndex = 0;
            if (args.Parameters.Count > 1)
                pageParamIndex = 1;
            if (!PaginationTools.TryParsePageNumber(args.Parameters, pageParamIndex, args.Player, out pageNumber))
                return;

            List<string> lines = new List<string> {
                          "start <quest name> - Starts quest",
                          "stop - stops current running quest",
                          "list - lists available quests at current Quest Region",
                          "progress - shows current quest progress",
                        };

            if (args.Player.Group.HasPermission(AdventureQuestsMain.QuestAdmin_Permission))
            {
                lines.Add("give <player> <quest> - gives a player a quest.");
                lines.Add("forceall <quest> - forces all players to do this quest.");
            }

            PaginationTools.SendPage(
              args.Player, pageNumber, lines,
              new PaginationTools.Settings
              {
                  HeaderFormat = "Available Quest Sub-Commands ({0}/{1}):",
                  FooterFormat = "Type /quest {0} for more sub-commands."
              }
            ); 
        }
        #endregion

        public static void GetCoords(CommandArgs args)
        {
            args.Player.SendMessage(string.Format("X: {0}, Y:{1}", args.Player.TileX, args.Player.TileY), Color.Yellow);
        }

        #region QuestRegion
        public static void QuestRegion(CommandArgs args)
        {
            if (args.Parameters.Count > 0)
            {
                switch (args.Parameters[0].ToLower())
                {
                    case "define":
                        {
                            QuestRegionDefine(args);
                            break;
                        }
                    case "add":
                        {
                            QuestRegionAdd(args);
                            break;
                        }
                    case "remove":
                        {
                            QuestRegionRemove(args);
                            break;
                        }
                    case "delete":
                        {
                            QuestRegionDelete(args);
                            break;
                        }
                    case "list":
                        {
                            QuestRegionList(args);
                            break;
                        }
                    case "entry":
                        {
                            QuestRegionEntry(args);
                            break;
                        }
                    case "exit":
                        {
                            QuestRegionExit(args);
                            break;
                        }
                    case "help":
                    default:
                        {
                            QuestRegionHelp(args);
                            break;
                        }
                }
            }
            else
                QuestRegionHelp(args);
        }

        private static void QuestRegionDefine(CommandArgs args) 
        {
            if (args.Parameters.Count < 2)
            {
                args.Player.SendMessage("Invalid usage: /questregion define [region name]", Color.Red);
                return;
            }

            string region = args.Parameters[1];
            if (TShock.Regions.GetRegionByName(region) == null)
            {
                args.Player.SendMessage("Region does not exist.", Color.Red);
                return;
            }
            if (AdventureQuestsMain.regions.AddRegion(region))
                args.Player.SendMessage("Region has been defined.", Color.Green);
            else
                args.Player.SendMessage("Region already defined.", Color.Red);

            args.Player.SendInfoMessage(string.Format("Quest Region: {0} defined", args.Parameters[1]));
        }

        private static void QuestRegionAdd(CommandArgs args)
        {
            if (args.Parameters.Count < 3)
            {
                args.Player.SendMessage("Invalid usage: /questregion add [quest name] [region name]", Color.Red);
                return;
            }
            QuestInfo qi = AdventureQuestsMain.quests.getQuest(args.Parameters[1]);
            if (qi == null)
            {
                args.Player.SendMessage(string.Format("Quest: {0} was not found", args.Parameters[1]), Color.Red);
                return;
            }

            QuestRegion qr = AdventureQuestsMain.regions.GetRegion(args.Parameters[2]);
            if (qr == null)
            {
                args.Player.SendMessage(string.Format("Quest Region: {0} was not found", args.Parameters[2]), Color.Red);
                return;
            }

            if(qr.AddQuest(qi))
                AdventureQuestsMain.regions.UpdateRegion(args.Parameters[2]);
            else
                args.Player.SendMessage(string.Format("Quest: {0} is already in Quest Region: {1}", args.Parameters[1], args.Parameters[2]), Color.Red);

            args.Player.SendInfoMessage(string.Format("Quest: {0} added into Quest Region: {1}", args.Parameters[1], args.Parameters[2]));
        }

        private static void QuestRegionRemove(CommandArgs args)
        {
            if (args.Parameters.Count < 3)
            {
                args.Player.SendMessage("Invalid usage: /questregion remove [quest name] [region name]", Color.Red);
                return;
            }

            QuestRegion qr = AdventureQuestsMain.regions.GetRegion(args.Parameters[2]);
            if (qr == null)
            {
                args.Player.SendMessage(string.Format("Quest Region: {0} was not found", args.Parameters[2]), Color.Red);
                return;
            }

            QuestInfo qi = AdventureQuestsMain.quests.getQuest(args.Parameters[1]);
            if (!qr.RemoveQuest(qi))
                args.Player.SendMessage(string.Format("Quest: {0} was not found in Quest Region: {1}", args.Parameters[1], args.Parameters[2]), Color.Red);
            else
                AdventureQuestsMain.regions.UpdateRegion(args.Parameters[2]);

            args.Player.SendInfoMessage(string.Format("Quest: {0} removed from Quest Region: {1}", args.Parameters[1], args.Parameters[2]));
        }

        private static void QuestRegionDelete(CommandArgs args)
        {
            if (args.Parameters.Count < 2)
            {
                args.Player.SendMessage("Invalid usage: /questregion delete [region name]", Color.Red);
                return;
            }
            QuestRegion qr = AdventureQuestsMain.regions.GetRegion(args.Parameters[1]);
            if (qr == null)
            {
                args.Player.SendMessage(string.Format("Quest Region: {0} was not found", args.Parameters[1]), Color.Red);
                return;
            }

            //TODO
        }

        private static void QuestRegionList(CommandArgs args)
        {
            int pageNumber;
            if (!PaginationTools.TryParsePageNumber(args.Parameters, 1, args.Player, out pageNumber))
                return;

            IEnumerable<string> regionNames = AdventureQuestsMain.regions.ListRegions();

            PaginationTools.SendPage(args.Player, pageNumber, PaginationTools.BuildLinesFromTerms(regionNames),
                new PaginationTools.Settings
                {
                    HeaderFormat = "Quest Regions ({0}/{1}):",
                    FooterFormat = "Type /questregion list {0} for more.",
                    NothingToDisplayString = "There are currently no quest regions defined."
                });
        }

        private static void QuestRegionEntry(CommandArgs args)
        {
            if (args.Parameters.Count < 3)
            {
                args.Player.SendMessage("Invalid usage: /questregion entry [region name] [message]", Color.Red);
                return;
            }

            QuestRegion qr = AdventureQuestsMain.regions.GetRegion(args.Parameters[1]);
            if (qr == null)
            {
                args.Player.SendMessage(string.Format("Quest Region: {0} was not found", args.Parameters[1]), Color.Red);
                return;
            }

            qr.entryMessage = Utilities.ParamsToSingleString(args, 2);
            AdventureQuestsMain.regions.UpdateRegion(args.Parameters[1]);

            args.Player.SendInfoMessage("Entry message updated");
        }

        private static void QuestRegionExit(CommandArgs args)
        {
            if (args.Parameters.Count < 3)
            {
                args.Player.SendMessage("Invalid usage: /questregion exit [region name] [message]", Color.Red);
                return;
            }

            QuestRegion qr = AdventureQuestsMain.regions.GetRegion(args.Parameters[1]);
            if (qr == null)
            {
                args.Player.SendMessage(string.Format("Quest Region: {0} was not found", args.Parameters[1]), Color.Red);
                return;
            }

            qr.exitMessage = Utilities.ParamsToSingleString(args, 2);
            AdventureQuestsMain.regions.UpdateRegion(args.Parameters[1]);

            args.Player.SendInfoMessage("Exit message updated");
        }

        private static void QuestRegionHelp(CommandArgs args)
        {
            int pageNumber;
            int pageParamIndex = 0;
            if (args.Parameters.Count > 1)
                pageParamIndex = 1;
            if (!PaginationTools.TryParsePageNumber(args.Parameters, pageParamIndex, args.Player, out pageNumber))
                return;

            List<string> lines = new List<string> {
                          "define <name> - Defines a Quest Region from a TShock Region",
                          "add <questname> <regionname> - Adds quest to region",
                          "remove <questname> <regionname> - Removes quest from region",
                          "delete <regionname> - Deletes the given Quest Region",
                          "entry <regionname> <message> - Changes the Entry Message of the Quest Region",
                          "exit <regionname> <message> - Changes the Exit Message of the Quest Region",
                          "list - Lists all Quest Regions",
                        };

            PaginationTools.SendPage(
              args.Player, pageNumber, lines,
              new PaginationTools.Settings
              {
                  HeaderFormat = "Available Quest Region Sub-Commands ({0}/{1}):",
                  FooterFormat = "Type /questregion {0} for more sub-commands."
              }
            ); 
        }
        #endregion

        public static void QuestReload(CommandArgs args)
        {
            AdventureQuestsMain.quests.ImportQuests();
            AdventureQuestsMain.ImportDatabase();
            args.Player.SendInfoMessage("Quests and Quest Regions have been reloaded");
        }
    }
}
