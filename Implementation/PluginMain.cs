﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using MySql.Data.MySqlClient;
using TShockAPI.DB;

using Terraria;
using TerrariaApi.Server;
using TShockAPI;

namespace AdventureQuests
{
    [ApiVersion(1, 16)]
    public class AdventureQuestsMain : TerrariaPlugin
    {
        #region Plugin Permissions
        public const string UseQuest_Permission = "quests.usequest";
        public const string QuestRegion_Permission = "quests.questregion";
        public const string QuestAdmin_Permission = "quests.admin";
        #endregion

        #region Plugin Variables
        public static QuestManager quests = new QuestManager();
        public static QuestPlayerManager players = new QuestPlayerManager();
        public static QuestRegionManager regions = new QuestRegionManager();

        public static IDbConnection db;

        public static AdventureQuestsMain instance;

        public static ThreadHandler threadHandler = new ThreadHandler();
        public static Thread thread;
        #endregion

        #region Plugin Overrides
        public override void Initialize()
        {
            ServerApi.Hooks.GameInitialize.Register(this, OnInitialize);
            ServerApi.Hooks.GamePostInitialize.Register(this, OnPostInitialize, -1);
            ServerApi.Hooks.NetGreetPlayer.Register(this, OnGreetPlayer);
            ServerApi.Hooks.ServerLeave.Register(this, OnLeave);
            ServerApi.Hooks.GameUpdate.Register(this, OnUpdate);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ServerApi.Hooks.GameInitialize.Deregister(this, OnInitialize);
                ServerApi.Hooks.GamePostInitialize.Deregister(this, OnPostInitialize);
                ServerApi.Hooks.NetGreetPlayer.Deregister(this, OnGreetPlayer);
                ServerApi.Hooks.ServerLeave.Deregister(this, OnLeave);
                ServerApi.Hooks.GameUpdate.Deregister(this, OnUpdate);

                threadHandler.running = false;
                thread.Abort();
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Plugin Hooks
        private void OnInitialize(EventArgs args)
        {
            Commands.ChatCommands.Add(new Command(UseQuest_Permission, QuestCommands.RootCommand, "quest"));
            Commands.ChatCommands.Add(new Command(UseQuest_Permission, QuestCommands.GetCoords, "getcoords"));
            Commands.ChatCommands.Add(new Command(QuestRegion_Permission, QuestCommands.QuestRegion, "questregion", "qregion"));
            Commands.ChatCommands.Add(new Command(QuestAdmin_Permission, QuestCommands.QuestReload, "questreload"));
            CheckDatabase();

            threadHandler.running = true;
            thread = new Thread(threadHandler.QuestHandler);
            thread.Start();
        }
        private void OnPostInitialize(EventArgs args)
        {
            quests.ImportQuests();
            ImportDatabase();
        }
        private void OnGreetPlayer(GreetPlayerEventArgs args)
        {
            players.AddPlayer(args.Who);
        }
        private void OnLeave(LeaveEventArgs args)
        {
            players.DelPlayer(args.Who);
        }
        private void OnUpdate(EventArgs args)
        {
            players.OnUpdate(args);
        }
        #endregion

        #region Plugin Methods
        private static void CheckDatabase()
        {
            if (TShock.Config.StorageType.ToLower() == "sqlite")
            {
                string sql = Path.Combine(TShock.SavePath, "AdventureQuests.sqlite");
                db = new SqliteConnection(string.Format("uri=file://{0},Version=3", sql));
            }
            else if (TShock.Config.StorageType.ToLower() == "mysql")
            {
                try
                {
                    var hostport = TShock.Config.MySqlHost.Split(':');
                    db = new MySqlConnection();
                    db.ConnectionString =
                        String.Format("Server={0}; Port={1}; Database={2}; Uid={3}; Pwd={4};",
                                      hostport[0],
                                      hostport.Length > 1 ? hostport[1] : "3306",
                                      TShock.Config.MySqlDbName,
                                      TShock.Config.MySqlUsername,
                                      TShock.Config.MySqlPassword
                            );
                }
                catch (MySqlException ex)
                {
                    Log.Error(ex.ToString());
                    throw new Exception("MySql not setup correctly");
                }
            }
            else
            {
                throw new Exception("Invalid storage type");
            }

            var table = new SqlTable("QuestRegions",
                                     new SqlColumn("Name", MySqlDbType.VarChar, 56) { Length = 56, Primary = true },
                                     new SqlColumn("Quests", MySqlDbType.Text) { DefaultValue = "" },
                                     new SqlColumn("EntryMessage", MySqlDbType.Text) { DefaultValue = "" },
                                     new SqlColumn("ExitMessage", MySqlDbType.Text) { DefaultValue = "" }
                );
            var creator = new SqlTableCreator(db,
                                              db.GetSqlType() == SqlType.Sqlite
                                                ? (IQueryBuilder)new SqliteQueryCreator()
                                                : new MysqlQueryCreator());
            creator.EnsureExists(table);

            //Add Quest Tracking
            //table = new SqlTable("QuestPlayers",
            //             new SqlColumn("Name", MySqlDbType.VarChar, 56) { Length = 56, Primary = true }
            //);
            //creator = new SqlTableCreator(db,
            //                                  db.GetSqlType() == SqlType.Sqlite
            //                                    ? (IQueryBuilder)new SqliteQueryCreator()
            //                                    : new MysqlQueryCreator());
            //creator.EnsureExists(table);
        }
        public static void ImportDatabase()
        {
            String query = "SELECT * FROM QuestRegions";

            using (var reader = db.QueryReader(query))
            {
                regions.Clear();

                while (reader.Read())
                {
                    string name = reader.Get<string>("Name");
                    string questStr = reader.Get<string>("Quests") ?? "";
                    string entryMessage = reader.Get<string>("EntryMessage") ?? "";
                    string exitMessage = reader.Get<string>("ExitMessage") ?? "";

                    List<string> questsStr = new List<string>(questStr.Split(',').ToList().Select(s => s.Trim()));

                    List<QuestInfo> questsList = new List<QuestInfo>();

                    QuestInfo qi;
                    foreach (string questName in questsStr){
                        qi = quests.getQuest(questName);
                        if (qi != null)
                            questsList.Add(qi);
                    }

                    regions.ImportRegion(name, questsList, entryMessage, exitMessage);
                }
            }
        }
        #endregion

        #region Plugin Properties
        public AdventureQuestsMain(Main game)
            : base(game)
        {
            instance = this;
            Order = 5;
        }
        public override string Name
        {
            get { return "AdventureQuests"; }
        }
        public override string Author
        {
            get { return "Stealownz"; }
        }
        public override string Description
        {
            get { return "Terraria Quests"; }
        }
        public override Version Version
        {
            get { return new Version(1, 0); }
        }
        #endregion
    }
}
