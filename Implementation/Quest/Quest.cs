﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Reflection;

using Microsoft.Scripting.Hosting;
using IronPython.Hosting;

using TShockAPI;

namespace AdventureQuests
{
    public class Quest
    {
        public QuestInfo info { get; private set; }
        public QuestPlayer player { get; private set; }
        //public QuestScript script { get; private set; }

        public LinkedList<Trigger> triggers{ get; private set; }
        public List<Trigger> completedTriggers = new List<Trigger>();
        public Trigger currentTrigger { get; protected set; }

        public string path { get { return this.info.Path; } }
        public bool running;
        public TimeSpan PauseTime;

        public ScriptEngine engine { get; private set; }
        public ScriptScope scope { get; private set; }

        public Quest(QuestInfo info, QuestPlayer player)
        {
            this.info = info;
            this.player = player;
            //this.script = new QuestScript(path, engine);

            engine = Python.CreateEngine();
            engine.Runtime.LoadAssembly(Assembly.GetExecutingAssembly());

            scope = engine.CreateScope();
            scope.SetVariable("Quest", this);
            scope.SetVariable("Player", this.player);

            RunQuest();
        }

        public void Add(Trigger trigger)
        {
            if (triggers.Last == null)
            {
                triggers.AddFirst(trigger);
                return;
            }
            triggers.AddLast(trigger);
        }

        public void Prioritize(Trigger trigger)
        {
            triggers.AddFirst(trigger);
        }

        public void PrioritizeAfter(Trigger trigger, int index)
        {
            try
            {
                if (index <= 0)
                    throw new FormatException("Index cannot be negative nor zero");

                LinkedListNode<Trigger> node = triggers.First;

                for (int i = 0; i < index -1; i++)
                {
                    if (node != null)
                        node = node.Next;
                    else
                        throw new FormatException("Index cannot be reached");
                }

                if (node == null)
                    throw new FormatException("Index is null");

                triggers.AddAfter(node, trigger);
            }
            catch(Exception e)
            {
                System.Text.StringBuilder errorMessage = new System.Text.StringBuilder();
                errorMessage.AppendLine(string.Format("Error in quest system while PrioritizeAfter: QuestName: {0}", this.path));
                errorMessage.AppendLine(e.Message);
                errorMessage.AppendLine(e.StackTrace);
                Log.ConsoleError(errorMessage.ToString());

                if (e.InnerException != null)
                {
                    Log.ConsoleError("Inner Exception:");
                    Log.ConsoleError(e.InnerException.ToString());
                }

                player.runningQuest = false;
                player.tsPlayer.SendErrorMessage("Your quest has failed to load due to an error in the script or the system. Please report this to a server administrator.");
            }
        }

        public void NextTrigger()
        {
            if (triggers.Count == 0)
            {
                currentTrigger = null;
            }
            else
            {
                currentTrigger = triggers.First.Value;
                triggers.RemoveFirst();
                currentTrigger.Initialize();
            }
        }

        public void EvaluateTrigger()
        {
            if (currentTrigger.Update(this))
            {
                currentTrigger.onComplete();

                completedTriggers.Add(currentTrigger);
                NextTrigger();
            }
        }

        public void ClearQueue()
        {
            this.triggers = new LinkedList<Trigger>();
            if (this.currentTrigger != null)
            {
                this.currentTrigger.onComplete();
                this.currentTrigger = null;
            }
        }

        private void RunQuest()
        {
            try
            {
                this.triggers = new LinkedList<Trigger>();

                //execute
                //!!!uncomment later.!!!
                //source.Execute(scope);

                StringBuilder sb = new StringBuilder();
                using (StreamReader sr = new StreamReader(this.path))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(line);
                    }
                }

                engine.Execute("import Triggers", scope);
                engine.Execute(sb.ToString(), scope);

                this.player.tsPlayer.SendInfoMessage(string.Format("Quest {0} has started.", this.info.Name));

                if (triggers.Count == 0)
                    throw new Exception(string.Format("The script for the quest \"{0}\" never enqueues any triggers. Quests must enqueue at least one trigger.", this.info.Name));

                running = true;

                currentTrigger = triggers.First.Value;
                currentTrigger.Initialize();
                triggers.RemoveFirst();
            }
            catch (Exception e)
            {
                System.Text.StringBuilder errorMessage = new System.Text.StringBuilder();
                errorMessage.AppendLine(string.Format("Error in quest system while loading quest: Player: {0} QuestName: {1}", this.player.tsPlayer.Name, this.path));
                errorMessage.AppendLine(e.Message);
                errorMessage.AppendLine(e.StackTrace);
                Log.ConsoleError(errorMessage.ToString());

                if (e.InnerException != null)
                {
                    Log.ConsoleError("Inner Exception:");
                    Log.ConsoleError(e.InnerException.ToString());
                }

                player.runningQuest = false;
                player.tsPlayer.SendErrorMessage("Your quest has failed to load due to an error in the script or the system. Please report this to a server administrator.");
            }
        }

        public void Stop(bool completed)
        {
            ClearQueue();
            running = false;
            player.runningQuest = false;
            player.currentQuest = null;

            if (completed)
            {
                player.tsPlayer.SendInfoMessage("Quest Completed");
                //quest.player.MyDBPlayer.QuestAttemptData.Find(x => x.QuestName == quest.info.Name).Complete = true;
            }
            else
                player.tsPlayer.SendInfoMessage("Quest is not completed and is stopped");
        }
    }
}