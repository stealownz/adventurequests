﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventureQuests
{
    public class QuestInfo
    {
        public string Name;
        public string Path;

        public QuestInfo(string name, string path)
        {
            this.Name = name;
            this.Path = path;
        }
    }
}
