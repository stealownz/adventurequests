﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace AdventureQuests
{
    public class QuestManager
    {
        private Dictionary<string, QuestInfo> possibleQuests;

        public QuestManager()
        {
            possibleQuests = new Dictionary<string, QuestInfo>();
        }

        public void ImportQuests()
        {
            possibleQuests = new Dictionary<string, QuestInfo>();
            if (!Directory.Exists("Quests"))
                Directory.CreateDirectory("Quests");
            string[] filePaths = Directory.GetFiles("Quests", "*.py");
            possibleQuests.Clear();
            foreach (string path in filePaths)
            {
                try
                {
                    string[] config = File.ReadAllLines(path.Split('.')[0] + ".cfg");

                    string name = null;
                    //string permission = "";
                    //int time = 0;
                    //TimeSpan interval = TimeSpan.Zero;

                    foreach (string line in config)
                    {
                        try
                        {
                            if (line.Trim().StartsWith("Name"))
                                name = line.Trim().Split('=')[1];
                            //if (line.Trim().StartsWith("Permission"))
                            //    permission = line.Trim().Split('=')[1];
                            //if (line.Trim().StartsWith("Time"))
                            //    time = Int32.Parse(line.Trim().Split('=')[1]);
                            //if (line.Trim().StartsWith("Interval"))
                            //    interval = TimeSpan.Parse(line.Trim().Split('=')[1]);
                        }
                        catch (FormatException e)
                        {
                            ConsoleColor buffer = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Error while loading quest {0}", name != null ? name : "(Error is in loading name)");
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.ForegroundColor = buffer;
                        }
                    }
                    if (!(name == null)) //Name is required. The rest are optional.
                        possibleQuests.Add(name, new QuestInfo(name, path));
                }
                catch { }
            }
        }

        public QuestInfo getQuest(string quest)
        {
            QuestInfo qi;
            possibleQuests.TryGetValue(quest, out qi);
            return qi;
        }

        public static void StartQuest(QuestPlayer ply, QuestInfo q)
        {
            ply.currentQuest = new Quest(q, ply);
            lock (AdventureQuestsMain.threadHandler.RunningQuests)
                AdventureQuestsMain.threadHandler.RunningQuests.Add(ply.currentQuest);
        }
    }
}
