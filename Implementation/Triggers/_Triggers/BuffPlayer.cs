﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class BuffPlayer : Trigger
    {
        private QuestPlayer player;
        private string buffname;
        private int time;

        public BuffPlayer(QuestPlayer player, string buffname, int time)
        {
            this.player = player;
            this.buffname = buffname;
            this.time = time;
        }

        public override bool Update(Quest q)
        {
            List<int> buffs = TShock.Utils.GetBuffByName(buffname);
            if (buffs.Count == 1)
            {
                player.tsPlayer.SetBuff(buffs[0], time * 60);
                return true;
            }
            else
            {
                throw new FormatException("More than one or no buffs matched to the name provided.");
            }
        }
    }
}
