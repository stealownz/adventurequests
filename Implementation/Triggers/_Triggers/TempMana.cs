﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class TempMana : Trigger
    {
        private QuestPlayer player;
        private int mana;

        public TempMana(QuestPlayer player, int mana)
        {
            this.player = player;
            this.mana = mana;
        }

        public override void Initialize()
        {
            if (this.player.OriginalData == null)
            {
                player.OriginalData = new PlayerData(player.tsPlayer);
                player.OriginalData.CopyCharacter(player.tsPlayer);
            }
        }

        public override bool Update(Quest q)
        {
            var newData = new PlayerData(player.tsPlayer);
            newData.CopyCharacter(player.tsPlayer);
            newData.mana = mana;
            newData.maxMana = mana;
            newData.RestoreCharacter(player.tsPlayer);
            return true;
        }
    }
}
