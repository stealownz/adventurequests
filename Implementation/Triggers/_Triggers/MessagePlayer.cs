﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TerrariaApi.Server;

using DColor = System.Drawing.Color;

namespace Triggers
{
    public class MessagePlayer : Trigger
    {
        private string message;
        private DColor color;
        private QuestPlayer player;

        public MessagePlayer(string message, QuestPlayer player, string color)
        {
            this.message = message;
            this.color = DColor.FromName(color);
            this.player = player;
        }

        public override bool Update(Quest q)
        {
            player.tsPlayer.SendMessage(message, color.R, color.G, color.B);
            return true;
        }
    }
}
