﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TShockAPI;

using AdventureQuests;

using TerrariaApi.Server;

namespace Triggers
{
    public class Teleport : Trigger
    {
        private QuestPlayer player;
        private int x, y;
        private string warpName;

        public Teleport(QuestPlayer player, int x, int y)
        {
            this.x = x;
            this.y = y;
            this.player = player;
            this.warpName = string.Empty;
        }

        public Teleport(QuestPlayer player, string warp)
        {
            this.x = -1;
            this.y = -1;
            this.player = player;
            this.warpName = warp;
        }

        public override bool Update(Quest q)
        {
            if (warpName == string.Empty)
                return player.tsPlayer.Teleport(x * 16 , (y + 3) * 16);
            else
            {
                var warp = TShock.Warps.Find(warpName);
                if (warp != null)
                {
                    return player.tsPlayer.Teleport(warp.Position.X * 16, warp.Position.Y * 16);
                }
                else
                {
                    throw new FormatException("Warp not found.");
                }
            }
        }
    }
}
