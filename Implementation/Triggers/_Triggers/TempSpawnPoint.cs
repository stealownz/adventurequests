﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class TempSpawnPoint : Trigger
    {
        private QuestPlayer player;
        private int x;
        private int y;

        public TempSpawnPoint(QuestPlayer player, int x, int y)
        {
            this.player = player;
            this.x = x;
            this.y = y;
        }

        public override void Initialize()
        {
            if (this.player.OriginalData == null)
            {
                this.player.OriginalData = new PlayerData(player.tsPlayer);
                this.player.OriginalData.CopyCharacter(player.tsPlayer);
            }
        }

        public override bool Update(Quest q)
        {
            var newData = new PlayerData(player.tsPlayer);
            newData.CopyCharacter(player.tsPlayer);
            newData.spawnX = x;
            newData.spawnY = y;
            newData.RestoreCharacter(player.tsPlayer);
            return true;
        }
    }
}
