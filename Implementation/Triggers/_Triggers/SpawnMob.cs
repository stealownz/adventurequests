﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using Terraria;
using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class SpawnMob : Trigger
    {
        private string name;
        private int x, y, amount;

        public override bool Update(Quest q)
        {
            List<NPC> npcs = TShock.Utils.GetNPCByIdOrName(name);
            if (npcs.Count == 1)
            {
                NPC npc = npcs[0];
                for (int i = 0; i < amount; i++)
                {
                    TSPlayer.Server.SpawnNPC(npc.type, npc.name, amount, x, y, 1, 1);
                }
                return true;
            }
            else
            {
                throw new FormatException("More than one or no NPCs matched to name or ID.");
            }
        }

        public SpawnMob(string name, int x, int y, int amount = 1)
        {
            this.x = x;
            this.y = y;
            this.amount = amount;
            this.name = name;
        }
    }
}
