﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TerrariaApi.Server;

namespace Triggers
{
    public class AtXY : Trigger
    {
        private int x, y, radius;
        private QuestPlayer player;
        private Rectangle playerRect { get { return new Rectangle(player.tsPlayer.TileX, player.tsPlayer.TileY, 1, 1); } }
        private Rectangle targetRect { get { return new Rectangle(x, y, radius, radius); } }

        public AtXY(QuestPlayer player, int x, int y, int radius = 1)
        {
            this.player = player;
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public override string Progress()
        {
            return string.Format("Destination: ({0}, {1}). Your position is: ({2},{3}).", x, y, player.tsPlayer.TileX, player.tsPlayer.TileY);
        }

        public override bool Update(Quest q)
        {
            return playerRect.Intersects(targetRect);
        }
    }
}
