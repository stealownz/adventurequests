﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using AdventureQuests;

using Terraria;
using TerrariaApi.Server;
using TShockAPI;

namespace Triggers
{
    public class GiveUpItem : Trigger
    {
        private Dictionary<string, int> toBeCollected = new Dictionary<string, int>();
        private QuestPlayer player;

        public override void Initialize()
        {
            ServerApi.Hooks.NetGetData.Register(AdventureQuestsMain.instance, this.checkItemDrops);
        }

        public override void onComplete()
        {
            ServerApi.Hooks.NetGetData.Deregister(AdventureQuestsMain.instance, this.checkItemDrops);
        }

        public override bool Update(Quest q)
        {
            if (toBeCollected.Count == 0)
            {
                return true;
            }
            return false;
        }

        public GiveUpItem(QuestPlayer player, string type, int amount = 1)
        {
            this.player = player;
            addItem(type, amount);
        }

        public void addItem(string type, int amount = 1)
        {
            List<Item> item = TShock.Utils.GetItemByIdOrName(type);
            if (item.Count == 1)
            {
                toBeCollected.Add(item[0].name, amount);
            }
            else
            {
                throw new FormatException("More than one or no items matched to name or ID.");
            }
        }

        public override string Progress()
        {
            StringBuilder progressText = new StringBuilder();
            progressText.Append("Items left to give: ");
            foreach (string key in toBeCollected.Keys)
            {
                progressText.AppendFormat("{0} ({1} Left), ", key, toBeCollected[key]);
            }
            return progressText.ToString();
        }

        private void checkItemDrops(GetDataEventArgs args)
        {
            if (!(player.tsPlayer.Index == args.Msg.whoAmI))
                return;

            if (args.MsgID == PacketTypes.ItemDrop)
            {
                if (args.Handled)
                    return;

                using (var data = new MemoryStream(args.Msg.readBuffer, args.Index, args.Length))
                {
                    int num = args.Index;

                    short num49 = BitConverter.ToInt16(args.Msg.readBuffer, num);
                    num += 2;
                    float posx = BitConverter.ToSingle(args.Msg.readBuffer, num);
                    num += 4;
                    float posy = BitConverter.ToSingle(args.Msg.readBuffer, num);
                    num += 4;
                    float velx = BitConverter.ToSingle(args.Msg.readBuffer, num);
                    num += 4;
                    float vely = BitConverter.ToSingle(args.Msg.readBuffer, num);
                    num += 4;
                    short stack = BitConverter.ToInt16(args.Msg.readBuffer, num);
                    num += 2;
                    byte prefix = args.Msg.readBuffer[num];
                    num++;
                    byte type = args.Msg.readBuffer[num];
                    num++;
                    short id = BitConverter.ToInt16(args.Msg.readBuffer, num);

                    Item item = new Item();
                    item.SetDefaults(id);

                    //Console.WriteLine(item.name);

                    if (id == 0)
                        return;

                    if (toBeCollected.ContainsKey(item.name))
                    {
                        toBeCollected[item.name] -= stack;

                        if (toBeCollected[item.name] < 0)
                        {
                            if (Math.Abs(toBeCollected[item.name]) > 1)
                                player.tsPlayer.SendInfoMessage(string.Format("Returning {0} {1}s", Math.Abs(toBeCollected[item.name]), item.name));
                            else
                                player.tsPlayer.SendInfoMessage(string.Format("Returning {0} {1}", Math.Abs(toBeCollected[item.name]), item.name));
                            args.Handled = true;
                            player.tsPlayer.GiveItem(item.type, item.name, item.width, item.width, Math.Abs(toBeCollected[item.name]));
                            toBeCollected.Remove(item.name);
                        }
                        else if (toBeCollected[item.name] > 0)
                        {
                            if (Math.Abs(toBeCollected[item.name]) > 1)
                                player.tsPlayer.SendInfoMessage(string.Format("Drop another {0} {1}s, to continue", Math.Abs(toBeCollected[item.name]), item.name));
                            else
                                player.tsPlayer.SendInfoMessage(string.Format("Drop {0} {1}, to continue", Math.Abs(toBeCollected[item.name]), item.name));
                            args.Handled = true;
                        }
                        else
                        {
                            if (stack > 1)
                                player.tsPlayer.SendInfoMessage(string.Format("You dropped {0} {1}s", stack, item.name));
                            else
                                player.tsPlayer.SendInfoMessage(string.Format("You dropped {0} {1}", stack, item.name));

                            toBeCollected.Remove(item.name);
                            args.Handled = true;
                        }
                    }
                    args.Handled = true;
                }
            }
        }
    }
	
}
