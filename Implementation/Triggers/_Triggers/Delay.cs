﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TerrariaApi.Server;

namespace Triggers
{
    public class Delay : Trigger
    {
        private TimeSpan DelayTime;

        public Delay(int seconds)
        {
            DelayTime = new TimeSpan(0, 0, seconds);
        }
        public override bool Update(Quest q)
        {
            q.PauseTime += DelayTime;
            return true;
        }
    }
}
