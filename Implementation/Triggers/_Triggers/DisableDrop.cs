﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class DisableDrop : Trigger
    {
        private QuestPlayer player;

        public DisableDrop(QuestPlayer player)
        {
            this.player = player;
        }

        public override bool Update(Quest q)
        {
            if (player.DropDisabled == false)
            {
                ServerApi.Hooks.NetGetData.Register(AdventureQuestsMain.instance, this.DisableDrops);
                player.DropDisabled = true;
            }
            else if (player.DropDisabled == true)
            {
                ServerApi.Hooks.NetGetData.Deregister(AdventureQuestsMain.instance, this.DisableDrops);
                player.DropDisabled = false;
            }
            return true;
        }
        private void DisableDrops(GetDataEventArgs args)
        {
            if (!(player.tsPlayer.Index == args.Msg.whoAmI))
                return;

            if (args.MsgID == PacketTypes.ItemDrop)
            {
                args.Handled = true;
            }
        }
    }
}
