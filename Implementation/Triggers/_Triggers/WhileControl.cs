﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AdventureQuests;

using Microsoft.Scripting.Hosting;

namespace Triggers
{
    public class WhileControl : Trigger
    {
        private string expression;
        private string originalExpression;

        public WhileControl(string expression)
        {
            string frontExpression = expression;
            this.originalExpression = expression;

            if (!frontExpression.Substring(0, frontExpression.IndexOf(" ")).Equals("while"))
                throw new FormatException("While not found in WhileControl Trigger.");
            else
                frontExpression = string.Concat(frontExpression.Substring(0, frontExpression.IndexOf(" ")).Replace("while", "if"),
                    frontExpression.Substring(frontExpression.IndexOf(" "))
                    );

            //add extra line trimming

            int whiteSpcCount = expression.Substring(expression.IndexOf("\n")).TakeWhile(c => char.IsWhiteSpace(c)).Count() - 1;
            string newWhile = "Quest.Prioritize(Triggers.WhileControl(\"\"\"{0}\"\"\"))";
            newWhile = newWhile.PadLeft(newWhile.Length + whiteSpcCount);
            newWhile = string.Format(newWhile, expression);

            this.expression = frontExpression.Insert(frontExpression.IndexOf("\n"), string.Concat(System.Environment.NewLine, newWhile));
        }

        public override bool Update(Quest q)
        {
            lock (q.triggers)
            {
                q.engine.Execute(expression, q.scope);
            }

            return true;
        }
    }
}
