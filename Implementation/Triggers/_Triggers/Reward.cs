﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using Terraria;
using TerrariaApi.Server;
using TShockAPI;

namespace Triggers
{
    public class Reward : Trigger
    {
        private QuestPlayer player;
        private Item item;

        public Reward(QuestPlayer player, string item, int stack = 1, int prefix = 0)
        {
            List<Item> items = TShock.Utils.GetItemByIdOrName(item);
            if (items.Count == 1)
            {
                this.item = items[0];
                this.player = player;
            }
            else
            {
                throw new FormatException("More than one or no items matched to name or ID.");
            }
            this.item.prefix = (byte)prefix;
            this.item.stack = stack;
        }

        public override bool Update(Quest q)
        {
            player.tsPlayer.GiveItem(item.type, item.name, item.width, item.height, item.stack, item.prefix);
            return true;
        }
    }
}
