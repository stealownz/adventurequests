﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using Microsoft.Scripting.Hosting;

namespace Triggers
{
    public class IfControl : Trigger
    {
        private string expression;

        public IfControl(string expression)
        {
            this.expression = expression;
        }

        public override bool Update(Quest q)
        {
            lock (q.triggers)
            {
                q.engine.Execute(expression, q.scope);
            }

            return true;
        }
    }
}
