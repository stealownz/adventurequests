﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.IO.Streams;

using AdventureQuests;

using TShockAPI;
using Terraria;
using TerrariaApi.Server;

namespace Triggers
{
    public class HuntMob : Trigger
    {
        private Dictionary<string, int> toBeKilled = new Dictionary<string, int>();
        private QuestPlayer player;

        public HuntMob(QuestPlayer Player, string type, int amount = 1)
        {
            player = Player;
            addNPC(type, amount);
        }

        public void addNPC(string type, int amount = 1)
        {
            List<NPC> npc = TShock.Utils.GetNPCByIdOrName(type);
            if (npc.Count == 1)
            {
                toBeKilled.Add(npc[0].name, amount);
            }
            else
            {
                throw new FormatException("More than one or no NPCs matched to name or ID.");
            }
        }

        public override string Progress()
        {
            StringBuilder progressText = new StringBuilder();
            progressText.Append("Monsters left to kill: ");
            foreach (string key in toBeKilled.Keys)
            {
                progressText.AppendFormat("{0} ({1} Left), ", key, toBeKilled[key]);
            }
            return progressText.ToString();
        }

        public override void Initialize()
        {
            ServerApi.Hooks.NetGetData.Register(AdventureQuestsMain.instance, this.OnGetData);
        }

        public override bool Update(Quest q)
        {
            if (toBeKilled.Count == 0)
                return true;
            return false;
        }

        public override void onComplete()
        {
            ServerApi.Hooks.NetGetData.Deregister(AdventureQuestsMain.instance, this.OnGetData);

        }

        public void OnGetData(GetDataEventArgs args)
        {
            if (args.Msg.whoAmI == player.tsPlayer.Index)
            {
                if (args.MsgID == PacketTypes.NpcStrike)
                {
                    MemoryStream data = new MemoryStream(args.Msg.readBuffer, args.Index, args.Length);
                    int npcID = data.ReadInt16();
                    int damage = data.ReadInt16();
                    if (Main.npc[npcID].life - damage <= 0)
                    {
                        if (!toBeKilled.ContainsKey(Main.npc[npcID].name))
                            return;
                        else
                            toBeKilled[Main.npc[npcID].name] -= 1;

                        if (toBeKilled[Main.npc[npcID].name] <= 0)
                            toBeKilled.Remove(Main.npc[npcID].name);
                    }
                }
            }
        }
    }
}
