﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class RestoreChar : Trigger
    {
        private QuestPlayer player;

        public RestoreChar(QuestPlayer player)
        {
            if (!TShock.Config.ServerSideCharacter)
            {
                return;
            }
            this.player = player;
        }
        public override bool Update(Quest q)
        {
            player.OriginalData.RestoreCharacter(player.tsPlayer);
            player.OriginalData = null;
            return true;
        }
    }
}
