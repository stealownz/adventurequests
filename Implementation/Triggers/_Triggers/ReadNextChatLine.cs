﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TerrariaApi.Server;

namespace Triggers
{
    public class ReadNextChatLine : Trigger
    {
        private QuestPlayer player;
        private string Message;

        public override void Initialize()
        {
            ServerApi.Hooks.ServerChat.Register(AdventureQuestsMain.instance, this.onChat);
        }

        public ReadNextChatLine(QuestPlayer player)
        {
            this.player = player;
        }

        public override void onComplete()
        {
            ServerApi.Hooks.ServerChat.Deregister(AdventureQuestsMain.instance, this.onChat);

        }

        public override bool Update(Quest q)
        {
            if (Message != null)
                return true;

            return false;
        }

        public void onChat(ServerChatEventArgs e)
        {
            if (e.Who == player.tsPlayer.Index)
            {
                if (Message == null)
                    Message = e.Text;
            }
        }
    }
}
