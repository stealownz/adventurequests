﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class TempHP : Trigger
    {
        private QuestPlayer player;
        private int hp;

        public TempHP(QuestPlayer player, int hp)
        {
            this.player = player;
            this.hp = hp;
        }

        public override void Initialize()
        {
            if (player.OriginalData == null)
            {
                player.OriginalData = new PlayerData(player.tsPlayer);
                player.OriginalData.CopyCharacter(player.tsPlayer);
            }
        }
        public override bool Update(Quest q)
        {
            var newData = new PlayerData(player.tsPlayer);
            newData.CopyCharacter(player.tsPlayer);
            newData.health = hp;
            newData.maxHealth = hp;
            newData.RestoreCharacter(player.tsPlayer);
            return true;
        }
    }
}
