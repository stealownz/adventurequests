﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using Terraria;
using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class GatherItem : Trigger
    {
        private Dictionary<string, int> toBeGathered = new Dictionary<string, int>();
        private Dictionary<string, int> itemInInv = new Dictionary<string, int>();
        private QuestPlayer player;

        public GatherItem(QuestPlayer player, string item, int amount = 1)
        {
            this.player = player;
            addItem(item, amount);
        }

        public override bool Update(Quest q)
        {
            foreach (string key in toBeGathered.Keys)
            {
                int buffer = itemInInv[key];
                try
                {
                    foreach (Item slot in player.tsPlayer.TPlayer.inventory)
                    {
                        if (slot != null)
                            if (slot.name == key)
                                buffer += slot.stack;
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                }

                if (buffer >= toBeGathered[key])
                {
                    toBeGathered.Remove(key);
                    itemInInv.Remove(key);
                }

                if (toBeGathered.Count == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public void addItem(string item, int amount = 1)
        {
            List<Item> matches = TShock.Utils.GetItemByName(item);
            if (matches.Count == 1)
            {
                Item match = matches[0];
                int count = 0;
                foreach (Item slot in player.tsPlayer.TPlayer.inventory)
                {
                    if (slot != null)
                        if (slot.name == match.name)
                            count -= slot.stack;
                }
                toBeGathered.Add(match.name, amount);
                itemInInv.Add(match.name, count);
            }
            else
            {
                throw new FormatException("More than one or no items matched to name or ID.");
            }
        }

        public override string Progress()
        {
            StringBuilder progressText = new StringBuilder();
            progressText.Append("Items left to gather: ");
            foreach (string key in toBeGathered.Keys)
            {
                progressText.AppendFormat("{0} ({1} Left), ", key, toBeGathered[key]);
            }
            return progressText.ToString();
        }
    }
}
