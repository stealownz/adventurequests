﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class ChangePlayerGroup : Trigger
    {
        private string targetGroup;
        private TSPlayer player;

        public ChangePlayerGroup(QuestPlayer player, string targetGroup)
        {
            this.player = player.tsPlayer;
            if (TShock.Groups.GroupExists(targetGroup))
            {
                this.targetGroup = targetGroup;
            }
            else
            {
                throw new FormatException(string.Format("The group {0} does not exist.", targetGroup));
            }
        }

        public override bool Update(Quest q)
        {
            TShock.Users.SetUserGroup(TShock.Users.GetUserByName(player.Name), targetGroup);
            return true;
        }
    }
}
