﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class TempInv : Trigger
    {
        private QuestPlayer player;
        private string option;

        public TempInv(QuestPlayer player, string option)
        {
            this.player = player;
            this.option = option;
        }

        public override void Initialize()
        {
            if (this.player.OriginalData == null)
            {
                this.player.OriginalData = new PlayerData(player.tsPlayer);
                this.player.OriginalData.CopyCharacter(player.tsPlayer);
            }
        }

        public override bool Update(Quest q)
        {
            if (this.option.ToLowerInvariant().Equals("Copy"))
            {
                var newData = new PlayerData(player.tsPlayer);
                newData.CopyCharacter(player.tsPlayer);
                newData.RestoreCharacter(player.tsPlayer);
            }
            else if (this.option.ToLowerInvariant().Equals("Clear"))
            {
                var newData = new PlayerData(player.tsPlayer);
                newData.CopyCharacter(player.tsPlayer);
                newData.inventory = NetItem.Parse("0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0~0,0,0");
                newData.RestoreCharacter(player.tsPlayer);
            }
            return true;
        }
    }
}
