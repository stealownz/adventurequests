﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureQuests;

using TShockAPI;
using TerrariaApi.Server;

namespace Triggers
{
    public class Broadcast : Trigger
    {
        private string message;
        private Color color;

        public Broadcast(string message, Color color)
        {
            this.message = message;
            this.color = color;
        }

        public override bool Update(Quest q)
        {
            TShock.Utils.Broadcast(message, color);
            return true;
        }
    }
}
