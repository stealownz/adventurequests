﻿namespace AdventureQuests
{
    public abstract class Trigger
    {
        public virtual void Initialize() { }
        public virtual bool Update(Quest q) { return true; }
        public virtual void onComplete() { }
        public virtual string Progress() { return ""; }
    }
}